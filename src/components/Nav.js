import React, { Component } from "react";
import logo from "./images/brisa-01-logo-black-and-white.png";

export class Nav extends Component {
    render() {
        return (
            <div className="navigation">
                <div className="nav-a">
                    <ul className="list-navigation">
                        <li>Home</li>
                        <li>Pages</li>
                        <li>Shop</li>
                        <li>Blog</li>
                        <li>Contact</li>
                    </ul>
                </div>
                <img src={logo} alt="logo" className="brand"></img>
                <div className="nav-b">
                    <ul className="list-navigation">
                        <li>Shops</li>
                        <li>Account</li>
                        <li>
                            <input
                                type="text"
                                className="nav-search"
                                placeholder="Search"
                            />
                        </li>
                        <li>Cart</li>
                    </ul>
                </div>
            </div>
        );
    }
}

export default Nav;
