import React, { Component } from "react";

export default class Section2 extends Component {
    render() {
        return (
            <div className="section2">
                <h1 className="section2-heading">What We Do</h1>
                <p className="section2-body">
                    Brisa is a leading edge fashion footwear specialist,
                    providing style conscious customers with innovative shoes to
                    suit every occassion. We pride ourderlves with our unique
                    product range - created by our in-house design team and
                    global brand partnerships, all of which are recognised by
                    their individuality, design and quality.
                </p>
                <button className="section2-button">
                    Learn More <img src=""></img>
                </button>
            </div>
        );
    }
}
