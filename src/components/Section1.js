import React, { Component } from "react";

export default class Section1 extends Component {
    render() {
        return (
            <div className="section1">
                {this.props.datas.slice(3, 4).map((data) => {
                    return (
                        <>
                            <div className="left-section1">
                                <h4 className="showcase-title">Just Droped</h4>
                                <h1 className="showcase-header">
                                    Feel Authentic Peace
                                </h1>
                                <button className="showcase-button">
                                    Shop Now
                                    <img
                                        src=""
                                        className="showcase-button-image"
                                    ></img>
                                </button>
                            </div>
                            <div className="right-section1">
                                <img
                                    src={data.image}
                                    alt={data.title}
                                    className="showcase-image"
                                ></img>
                            </div>
                        </>
                    );
                })}
            </div>
        );
    }
}
