import React, { Component } from "react";

export default class Section3 extends Component {
    render() {
        return (
            <div className="section3">
                <h1 className="section-heading">Featured Collections</h1>
                <div className="section3-items">
                    {this.props.datas.slice(0, 4).map((data) => {
                        return (
                            <div className="item">
                                <img
                                    src={data.image}
                                    alt="Product"
                                    className="product"
                                ></img>
                                <div className="section3-info">
                                    <h5 className="section3-info-title">
                                        {data.title}
                                    </h5>
                                    <p className="section3-info-price">
                                        ${data.price}
                                    </p>
                                </div>
                            </div>
                        );
                    })}
                </div>
            </div>
        );
    }
}
