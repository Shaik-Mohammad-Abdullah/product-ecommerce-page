import React, { Component } from "react";

export default class Section5 extends Component {
    render() {
        return (
            <div className="section5">
                {this.props.datas.slice(14, 15).map((data) => {
                    return (
                        <>
                            <img
                                src={data.image}
                                className="product section5-product"
                            ></img>
                            <div className="post">
                                <h4 className="post-title">True Story</h4>
                                <p className="post-description">
                                    "We pride ourselves on our unique product
                                    range - created by our in-house design team
                                    and global brand partnerships, all of which
                                    are individuality, design and quality."
                                </p>
                                <div className="post-comment">
                                    <span className="author">
                                        Julia Crawford
                                    </span>
                                    <span className="studio">
                                        Designer at Orizon Studio
                                    </span>
                                </div>
                            </div>
                        </>
                    );
                })}
            </div>
        );
    }
}
