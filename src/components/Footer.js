import React, { Component } from "react";
import instagram from "./images/instagram.jpeg";
import linkedin from "./images/linkedin.png";
import twitter from "./images/twitter.png";

export default class Footer extends Component {
    render() {
        return (
            <div className="footer">
                <h1>BRISA</h1>
                <div className="footer-column">
                    <ul className="list-footer">
                        <li>Shop</li>
                        <li>Lookbook</li>
                        <li>Legal</li>
                    </ul>
                </div>
                <div className="footer-column">
                    <ul className="list-footer">
                        <li>About</li>
                        <li>Privacy Policy</li>
                        <li>Terms and Condition</li>
                    </ul>
                </div>
                <div className="footer-column">
                    <ul className="list-footer">
                        <li>Subscribe to our newsletter</li>
                        <input
                            type="email"
                            className="email-input"
                            placeholder="Your Email"
                        ></input>
                    </ul>
                </div>
                <div className="footer-column">
                    <ul className="list-footer">
                        <li>Social Media</li>
                        <li>
                            <span>
                                <img
                                    src={twitter}
                                    className="twitter"
                                    alt="Twitter"
                                ></img>
                            </span>
                            <span>
                                <img
                                    src={instagram}
                                    className="instagram"
                                    alt="Instagram"
                                ></img>
                            </span>
                            <span>
                                <img
                                    src={linkedin}
                                    className="linkedin"
                                    alt="LinkedIn"
                                ></img>
                            </span>
                        </li>
                    </ul>
                </div>
            </div>
        );
    }
}
