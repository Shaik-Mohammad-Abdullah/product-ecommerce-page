import React, { Component } from "react";

export default class Section4 extends Component {
    render() {
        return (
            <div className="section4">
                <h1 className="section-heading">Popular Collection</h1>
                <div className="section4-items">
                    {this.props.datas.slice(0, 4).map((data) => {
                        return (
                            <div className="item">
                                <img
                                    src={data.image}
                                    alt="Product"
                                    className="product"
                                ></img>
                                <div className="section4-info">
                                    <h5 className="section4-info-title">
                                        {data.title}
                                    </h5>
                                    <p className="section4-info-price">
                                        ${data.price}
                                    </p>
                                </div>
                            </div>
                        );
                    })}
                </div>

                <div className="section4-items">
                    {this.props.datas.slice(14, 18).map((data) => {
                        return (
                            <div className="item">
                                <img
                                    src={data.image}
                                    alt="Product"
                                    className="product"
                                ></img>
                                <div className="section4-info">
                                    <h5 className="section4-info-title">
                                        {data.title}
                                    </h5>
                                    <p className="section4-info-price">
                                        ${data.price}
                                    </p>
                                </div>
                            </div>
                        );
                    })}
                </div>
            </div>
        );
    }
}
