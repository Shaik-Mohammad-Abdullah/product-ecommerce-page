import React, { Component } from "react";

export default class Loader extends Component {
    render() {
        return (
            <div className="loader">
                <ol>
                    <li>Loading...</li>
                </ol>
            </div>
        );
    }
}
