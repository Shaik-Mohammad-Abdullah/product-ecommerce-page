import React, { Component } from "react";
import Nav from "./components/Nav";
import Section1 from "./components/Section1";
import Section2 from "./components/Section2";
import Section3 from "./components/Section3";
import Section4 from "./components/Section4";
import Section5 from "./components/Section5";
import Footer from "./components/Footer";
import Loader from "./components/Loader";
import axios from "axios";
import "./App.css";

export default class App extends Component {
    constructor() {
        super();
        this.state = {
            data: [],
            isDataLoaded: false,
        };
    }

    data = async () => {
        const dataObject = await axios.get("https://fakestoreapi.com/products");
        const data = dataObject.data;
        this.setState({
            data,
            isDataLoaded: true,
        });
    };

    componentDidMount = () => {
        this.data();
    };

    render() {
        const { isDataLoaded, data } = this.state;
        if (!isDataLoaded) {
            return <Loader />;
        }
        return (
            <div className="App">
                <Nav />
                <Section1 datas={data} />
                <Section2 />
                <Section3 datas={data} />
                <Section4 datas={data} />
                <Section5 datas={data} />
                <Footer />
            </div>
        );
    }
}
